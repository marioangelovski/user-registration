<?php
use Phppot\Member;

if (! empty($_POST["login-btn"])) {
    require_once __DIR__ . '/Model/Member.php';
    $member = new Member();
    $loginResult = $member->loginMember();
}
?>
<HTML>
<HEAD>
<TITLE>Login</TITLE>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/phppot-style.css" type="text/css"
	rel="stylesheet" />
<link href="assets/css/user-registration.css" type="text/css"
	rel="stylesheet" />
<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>
</HEAD>
<BODY>
	<nav class="navigation">
		
		<img src="assets/img/pngnft.png" alt="" id="logo">
			<div id="navigation">
				<a href="privacy.html">Pirvacy Policy</a>
				<a href="#">About</a>
				<a href="user-registration.php" class="nav-item" id="signupBtn">Get Started</a>
				<a href="login.php" id="loginBtn" class="nav-item"><img src="assets/img/computer-icons-login-download-exit-bb54dbaa996ef54e6ed3849c96baefd7" alt="" id="imglogin">Login</a>
				</div>

			<div class="mobile-nav">
				<input type="checkbox" id="sub-nav">
				<label for="sub-nav" class="hamb"><span class="hamb-line"></span></label>
				<div id="sub-navigation">
					<a href="#" class="nav-item notBtn">Privacy Policy</a>
					<a href="#" class="nav-item notBtn">About</a>
					<a href="user-registration.php" class="nav-item" id="signupBtn">Get Started</a>
					<a href="login.php" id="loginBtn" class="nav-item"><img src="assets/img/computer-icons-login-download-exit-bb54dbaa996ef54e6ed3849c96baefd7" alt="" id="imglogin">Login</a>
				</div>

			</div>
		</nav>
	<div class="phppot-container">
		<div class="sign-up-container">
			
			<div class="signup-align">
				<form name="login" action="" method="post"
					onsubmit="return loginValidation()">
					<div class="signup-heading">Login</div>
				<?php if(!empty($loginResult)){?>
				<div class="error-msg"><?php echo $loginResult;?></div>
				<?php }?>
				<div class="row">
						<div class="inline-block">
							<div class="form-label">
								Username<span class="required error" id="username-info"></span>
							</div>
							<input class="input-box-330" type="text" name="username"
								id="username">
						</div>
					</div>
					<div class="row">
						<div class="inline-block">
							<div class="form-label">
								Password<span class="required error" id="login-password-info"></span>
							</div>
							<input class="input-box-330" type="password"
								name="login-password" id="login-password">
						</div>
					</div>
					<div class="row">
						<div class="inline-block">
							<?php
								// Generating a random number
								$randomNumber = rand();
							?>
							<div class="form-label required error" >Insert The following number <br> <br><div class="random-number"><?php print_r( $randomNumber);
								print_r("\n");
								
								?></div>
								<input class="input-box-330" type="text" name="recapcha"
									id="recapcha" placeholder="Recapcha">
							</div>
					</div>
					<p class="infoP">If you experience problems logging with your account please Contact support</p>
					<div class="row">
						<input class="btn" type="submit" name="login-btn"
							id="login-btn" value="Login">
					</div>
					<div class="row">
					<div >
						<a href="user-registration.php" class="login-signup">Sign up</a>
					</div>
				</div>
				
				</form>
			
			</div>
		</div>
	</div>			
	<script>
function loginValidation() {
	var valid = true;
	$("#username").removeClass("error-field");
	$("#password").removeClass("error-field");

	var UserName = $("#username").val();
	var Password = $('#login-password').val();

	$("#username-info").html("").hide();

	if (UserName.trim() == "") {
		$("#username-info").html("required.").css("color", "#ee0000").show();
		$("#username").addClass("error-field");
		valid = false;
	}
	if (Password.trim() == "") {
		$("#login-password-info").html("required.").css("color", "#ee0000").show();
		$("#login-password").addClass("error-field");
		valid = false;
	}
	if (valid == false) {
		$('.error-field').first().focus();
		valid = false;
	}
	return valid;
}
</script>
</BODY>
</HTML>
